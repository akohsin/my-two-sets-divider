import javax.swing.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.function.ToIntFunction;

public class Divider {
    private List<Integer> outputArray1 = new ArrayList<>();
    private List<Integer> outputArray2 = new ArrayList<>();
    private List<Integer> lista = new ArrayList<Integer>();

    public Divider(Integer... bat) {
        boolean isMove = false;

        for (Integer x : bat) {
            lista.add(x);
        }


        do {
            isMove = false;
            for (int i = 1; i < lista.size(); i++) {

                if (lista.get(i).compareTo(lista.get(i - 1)) > 0) {
                    Integer tmp1 = lista.get(i - 1);
                    lista.set(i - 1, lista.get(i));
                    lista.set(i, tmp1);
                    isMove = true;

                }

            }
            System.out.println(lista);
        } while (isMove);
    }

    public void compare() {
        outputArray1.add(lista.get(0));
        outputArray2.add(lista.get(1));
        for (int i = 2; i < lista.size(); i++) {
            int suma1 = summElements(outputArray1);
            int suma2 = summElements(outputArray2);
            int roznica = suma1 - suma2;
            if (roznica > 0) outputArray2.add(lista.get(i));
            else outputArray1.add(lista.get(i));
        }
    }

    private int summElements(List<Integer> x) {
        int sum = 0;
        for (Integer y : x) {
            sum += java.lang.Integer.parseInt(y.toString());
        }
        return sum;

    }

    public void printer() {
        System.out.print("tablica 1: ");
        for (Integer x : outputArray1) {
            System.out.print(x + " ");
        }
        System.out.println();
        System.out.print("tablica 2: ");
        for (Integer x : outputArray2) {
            System.out.print(x + " ");
        }
        System.out.println();
        System.out.println(summElements(outputArray1));
        System.out.println(summElements(outputArray2));
    }

}